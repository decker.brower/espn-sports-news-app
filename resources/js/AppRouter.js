var ESPN = ESPN || {};
ESPN.routers = ESPN.routers || {};
(function(APP, $, Backbone){
	APP.routers.AppRouter = Backbone.Router.extend({
		
		routes: {
			"" : "news"
		},

		initialize: function() {

		},

		news: function() {
			APP.newsView = new APP.views.NewsView();
			APP.newsView.render();
		}

	});
})(ESPN, jQuery, Backbone);
/*
Example Usage:

    $("#newsWrapper").MyDialog({
        title: "The Dialog Box",
        borderColor: "#e6e",
        message: "This is a Dialog Box"
    });
*/
(function($)
{
    // Convenient Inheritance
    Function.prototype.inheritsFrom = function(parentClassOrObject)
    {
        if (parentClassOrObject.constructor == Function)
        {
            // Normal Inheritance 
            this.prototype = new parentClassOrObject;
            this.prototype.constructor = this;
            this.prototype.parent = parentClassOrObject.prototype;
        }
        else
        {
            // Pure Virtual Inheritance 
            this.prototype = parentClassOrObject;
            this.prototype.constructor = this;
            this.prototype.parent = parentClassOrObject;
        }
        return this;
    };

    /*
    * Class: MyDialog
    * Parent Class: None
    */
    function Dialog() { return true; };

    Dialog.fn = Dialog.prototype;

    Dialog.createInstance = function(settings)
    {
        var instance = new Dialog();

        instance.init(settings);

        return instance;
    };

    Dialog.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.message = settings.message;
        this.buttonsArray = [];
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
        this.$dialogWrapper = $("<div id='Dialog-Box-Wrapper'></div>");
        this.$header = $("<div id='Dialog-Box-Wrapper-Header'></div>");
        this.$body = $("<div id='Dialog-Box-Wrapper-Body'></div>");
        this.$footer = $("<div id='Dialog-Box-Wrapper-Footer'></div>");
        this.$mask = $("<div id='Dialog-Box-Mask'></div>");

        this.$header.append(this.title);
        this.$body.append(this.message);
    };

    Dialog.fn.open = function()
    {
        for(var n = 0; n < this.buttonsArray.length; n++)
        {
            this.$footer.append(this.buttonsArray[n]);
        };

        this.$dialogBox.css({
            borderTop: "5px solid " + this.borderColor,
            borderBottom: "5px solid " + this.borderColor
        });

        this.$dialogWrapper.append(this.$header, this.$body, this.$footer);
        this.$dialogBox.append(this.$dialogWrapper);
        this.$target.parent().append(this.$mask);
        this.$target.append(this.$dialogBox);
    };

    Dialog.fn.createButton = function(options)
    {
        options = options || {};

        if (options.constructor.name !== "Object")
            options = {};

        var settings = $.extend({
            text: "Cancel",
            id: "jQuery-Dialog-Button",
            classes: ["Dialog-Button", "Cancel-Button"]
        }, options);

        var $button = $("<button></button>");

        $button.attr("id", settings.id);
        $button.append(settings.text);

        for( var i = 0; i < settings.classes.length; i++)
        {
            $button.addClass(settings.classes[i]);
        };

        return $button;
    };

    Dialog.fn.addButton = function($button)
    {
        this.buttonsArray.push($button);
    };

    Dialog.fn.destroy = function()
    {
        this.$dialogBox.remove();
        this.$mask.remove();
    };

    /*
    * Class: Cash
    * Parent Class: Dialog
    */
    function Cash() { return true; };

    Cash.inheritsFrom(Dialog);

    Cash.fn = Cash.prototype;

    Cash.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
    };

    /*
    * Class: WatchList
    * Parent Class: Dialog
    */
    function WatchList() { return true; };

    WatchList.inheritsFrom(Dialog);

    WatchList.fn = WatchList.prototype;

    WatchList.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
    };

    /*
    * Class: Equity
    * Parent Class: Dialog
    */
    function Equity() { return true; };

    Equity.inheritsFrom(Dialog);

    Equity.fn = Equity.prototype;

    Equity.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
    };

    /*
    * Class: Buy
    * Parent Class: Dialog
    */
    function Buy() { return true; };

    Buy.inheritsFrom(Dialog);

    Buy.fn = Buy.prototype;

    Buy.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
    };

    /*
    * Class: Sell
    * Parent Class: Dialog 
    */
    function Sell() { return true; };

    Sell.inheritsFrom(Dialog);

    Sell.fn = Sell.prototype;

    Sell.fn.init = function(settings)
    {
        this.title = settings.title;
        this.borderColor = settings.borderColor;
        this.$target = settings.$target;
        this.$dialogBox = $("<div id='Dialog-Box'></div>");
    };

    /*
    * Plugin: MyDialog
    */
    $.fn.MyDialog = function(options)
    {
        options = options || {};

        if (options.constructor.name !== "Object")
            options = {};

        var settings = $.extend(true, {
            title: "Dialog",
            borderColor: "#000000",
            $target: this,
            message: ""
        }, options);

        var dialog = Dialog.createInstance(settings);
        var $closeButton = dialog.createButton({text: "Close"});

        $closeButton.click(function(){dialog.destroy();});
        dialog.addButton($closeButton);
        dialog.open();
    };
})(jQuery);
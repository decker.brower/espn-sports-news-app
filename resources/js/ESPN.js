var ESPN = (function()
{
	var obj = {};

	obj.CurrentObj = {};
	obj.NewsObjs = [];

	obj.error = function(msg, cb)
	{
		msg = msg || "There was an error.";
		cb = cb || function() { return true; };

		var $errorBox = $("<div id='error-box'></div>");
		var $errorWrapper = $("<div id='error-wrapper'><p>" + msg + "</p></div>");
		var $closeErrorBtn = $("<button id='close-error-btn'>Ok</button>")
		.click(function()
		{
			$errorBox.remove();
			cb();
		});

		$errorWrapper.append($closeErrorBtn)
		$errorBox.append($errorWrapper);
		$("#newsWrapper").append($errorBox);
	};

	obj.shadeColor = function(color, percent) {   
		var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, B = (num >> 8 & 0x00FF) + amt, G = (num & 0x0000FF) + amt;
		return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
	};

	return obj;
})();

(function(NameSpace)
{
	var News = function() { return true; };

	News.createInstance = function(sport)
	{
		sport = sport || "";

		var Instance = new NameSpace.News();

		Instance.init(sport);

		return Instance;
	};

	News.fn = News.prototype;

	News.fn.init = function(sport, league, team, athlete)
	{
		sport = sport || "";
		league = league || "";
		team = team || "";
		athlete = athlete || "";

		this.sport = sport;
		this.league = league;
		this.team = team;
		this.athlete = athlete;
		this.leagueArray = [];
		this.teamArray = [];
		this.team = "";
		this.athlete = "";
		this.headlines = [];
		this.gotNews = false;
		this.gotLeagues = false;
		this.gotTeams = false;
		this.gotAthletes = false;
		this.filterTitleSport = "All Sports";
		this.filterTitleLeague = "";
		this.filterTitleTeam = "";
		this.teamColor = "#000";
		NameSpace.NewsObjs.push(this);
	};

	News.fn.getNews = function(newLeague, newTeam, cb)
	{
		newLeague = newLeague || "";
		newTeam = newTeam || "";
		cb = cb || function() { return true; };

		$("#newsWrapper").append("<div class='loading-screen'></div>");

		if (this.sport === "All Sports/")
		{
			this.sport = "";
			this.league = "";
			this.team = "";
			this.filterTitleLeague = "";
			this.filterTitleTeam = "";
			this.teamColor = "#000";
		}

		if (newTeam != "")
			newTeam = "teams=" + newTeam + "&";

		if (this.gotNews && newLeague == this.league && newTeam == this.team)
		{
			$("#headlines").remove();
			$("#newsWrapper .loading-screen").remove();
			this.buildList(cb);
		}
		else
		{
			var Instance = this;

			$.ajax({
				type: "GET",
				url: "http://api.espn.com/v1/sports/" + this.sport + newLeague + "news/?" + newTeam + "disable=audio%2C+mobileStory&apikey=9yc5khepquzfsvy599w2mc7n",
				dataType: "jsonp",
				success: function(result)
				{
					if (typeof result.headlines == "undefined")
					{
						$("#newsWrapper .loading-screen").remove();
						NameSpace.error("Oops! there are currently no headlines available... try again in a few seconds.", cb);
						return false;
					}

					if(result.headlines.length < 1)
					{
						$("#newsWrapper .loading-screen").remove();
						NameSpace.error("Oops! ESPN currently has no headlines available for your selected filters... try again later.", cb);
						return false;
					}

					Instance.headlines = result.headlines;
					$("#headlines").remove();
					$("#newsWrapper .loading-screen").remove();
					Instance.gotNews = true;
					Instance.league = newLeague;
					Instance.team = newTeam;
					Instance.buildList(cb);
				},
				error: function()
				{
					$("#newsWrapper .loading-screen").remove();
					NameSpace.error("Oops! the request failed... try again in a few seconds.", cb);
				}
			});
		}

		NameSpace.CurrentObj = this;
	};

	News.fn.getAllLeagues = function(cb)
	{
		cb = cb || function() { return true; };

		var Instance = this;

		if (Instance.gotLeagues)
		{
			cb();
			return true;
		}

		$.ajax({
			type: "GET",
			url: "http://api.espn.com/v1/sports/" + Instance.sport + "?disable=audio%2C+mobileStory&apikey=9yc5khepquzfsvy599w2mc7n",
			dataType: "jsonp",
			success: function(result)
			{
				
				$("#filter-box").children(".loading-screen").remove();

				if (typeof result.sports == "undefined")
				{
					NameSpace.error("Oops! ESPN didn't return anything... try again in a few seconds.");
					return false;
				}

				if (typeof result.sports[0].leagues == "undefined")
				{
					return false;
				}

				if (result.sports[0].leagues.length < 2)
				{
					cb();
					return false;
				}

				for (var i = 0; i < result.sports[0].leagues.length; i++)
				{
					var league = result.sports[0].leagues[i];

					Instance.leagueArray.push([league.name, league.abbreviation, league.groupId]);
				}

				Instance.gotLeagues = true;
				cb();
			},
			error: function(result, status, error)
			{
				NameSpace.error("Oops! the request failed... try again in a few seconds.", cb);
			}
		});
	};

	News.fn.getAllTeams = function(sport, league, cb)
	{
		sport = sport || "";
		league = league || "";
		cb = cb || function() { return true; };

		var Instance = this;

		if (Instance.gotTeams && Instance.league == league)
		{
			cb();
			return true;
		}

		$.ajax({
			type: "GET",
			url: "http://api.espn.com/v1/sports/" + sport + league + "/teams/?disable=audio%2C+mobileStory&apikey=9yc5khepquzfsvy599w2mc7n",
			dataType: "jsonp",
			success: function(result)
			{
				$("#filter-box").children(".loading-screen").remove();

				Instance.teamArray = [];

				for (var i = 0; i < result.sports[0].leagues[0].teams.length; i++)
				{
					var team = result.sports[0].leagues[0].teams[i];

					Instance.teamArray.push([team.name, team.location, team.id, team.color]);
				}

				Instance.gotTeams = true;
				cb();
			},
			error: function(result, status, error)
			{
				NameSpace.error("Oops! the request failed... try again in a few seconds.", cb);
			}
		});
	};

	News.fn.buildList = function(cb)
	{
		var $list = $("<div id='headlines'></div>");

		for (var i = 0; i < this.headlines.length; i++)
		{
			var news = this.headlines[i];

			if (typeof news.description == "undefined")
				news.description = "N/A";

			var $newsItem = $("<div class='news-item'></div>");
			var $headline = $("<h4><a href='" + news.links.web.href + "' target='_blank' title='Go to the article'>" + news.headline + "</a></h4>");
			var $description = $("<div class='description'></div>").hide();
			var $descriptionText = $("<p>" + news.description + "</p>");
			var $readMore = $("<button class='read-more'>Read More</button>")
			.click(function()
			{
				if ($(this).hasClass("active"))
				{
					$(this).parent().siblings(".description").slideUp();
					$(this).html("Read More");
					$(this).removeClass("active");
				}
				else
				{
					$(this).parent().siblings(".description").slideDown();
					$(this).html("Hide");
					$(this).addClass("active");
				}
			});

			if (news.images.length > 0)
			{
				var $sliderWrapper = $("<div class='slider-wrapper theme-default'><div class='ribbon'></div></div>");
				var $imagesContainer = $("<div id='slider' class='nivoSlider'></div>");
				var imageCount = 0;

				for (var x = 0; x < news.images.length; x++)
				{
					if(parseInt(news.images[x].width) < 200)
						continue;
					
					var $image = $(
						"<img src='" + news.images[x].url + "' title='" + news.images[x].caption +
						"' alt='" + news.images[x].alt + "' />"
					);

					$imagesContainer.append($image);
					imageCount++;
				}

				if(imageCount == 0)
					imageCount = "";

				$headline.append("<span class='image-count'>" + imageCount + " images</span>");

				/*
				options for effect:
				sliceDown
				sliceDownLeft
				sliceUp
				sliceUpLeft
				sliceUpDown
				sliceUpDownLeft
				fold
				fade
				random
				slideInRight
				slideInLeft
				boxRandom
				boxRain
				boxRainReverse
				boxRainGrow
				boxRainGrowReverse
				*/

				$imagesContainer.nivoSlider({
					effect: "boxRainGrowReverse", // Specify sets like: 'fold,fade,sliceDown'
					//slices: 15, // For slice animations
					boxRows: 4, // For box animations
					boxCols: 8, // For box animations
					animSpeed: 500, // Slide transition speed
					pauseTime: 10000 // How long each slide will show
					//startSlide: 0, // Set starting Slide (0 index)
					//directionNav: true, // Next & Prev navigation
					//controlNav: true, // 1,2,3... navigation
					//controlNavThumbs: false, // Use thumbnails for Control Nav
					//pauseOnHover: true, // Stop animation while hovering
					//manualAdvance: false, // Force manual transitions
					//prevText: 'Prev', // Prev directionNav text
					//nextText: 'Next', // Next directionNav text
					//randomStart: false, // Start on a random slide
					//beforeChange: function(){}, // Triggers before a slide transition
					//afterChange: function(){}, // Triggers after a slide transition
					//slideshowEnd: function(){}, // Triggers after all slides have been shown
					//lastSlide: function(){}, // Triggers when last slide is shown
					//afterLoad: function(){} // Triggers when slider has loaded
				});

				$sliderWrapper.append($imagesContainer);
				$description.append($sliderWrapper);
			}

			$headline.append($readMore);
			$description.append($descriptionText);
			$newsItem.append($headline, $description);

			$list.append($newsItem);
			$("#newsWrapper").append($list);

			$("#filter-title-sport").html(this.filterTitleSport);

			if(this.filterTitleLeague != "")
			{
				$("#filter-title-sport").html("<span id='filter-title-league'>" + this.filterTitleLeague + "</span>");
			}

			if(this.filterTitleTeam != "")
			{
				$("#filter-title-sport").append(
					"<br/><span id='filter-title-team' style='color:" + NameSpace.shadeColor(this.teamColor, 50) + ";text-shadow: 0px 0px 10px " + this.teamColor +
					";'> " + this.filterTitleTeam + 
					"</span>"
				);
				$("#filter-title-sport").attr("style", "color:" + NameSpace.shadeColor(this.teamColor, 50) + ";text-shadow: 0px 0px 10px " + this.teamColor + ";");
				$("#headlines a").attr("style", "color:" + NameSpace.shadeColor(this.teamColor, 50) + ";");
				$("#headlines .news-item .description p").attr("style", "color:" + NameSpace.shadeColor(this.teamColor, 45) + ";text-shadow: 0px 0px 10px " + this.teamColor + ";");
				$("#newsWrapper").attr(
					"style", "background: " + this.teamColor +
					";background: -webkit-gradient(linear, left top, left bottom, color-stop(0%," + 
					NameSpace.shadeColor(this.teamColor, 20) + "), color-stop(100%," + this.teamColor + "));" +
					"background: -moz-linear-gradient(top,  " + 
					NameSpace.shadeColor(this.teamColor, 20) + " 0%, " + this.teamColor + " 100%);"
				);
			}
			else
			{
				$("#filter-title-sport").attr("style", "");
				$("#headlines a").attr("style", "");
				$("#headlines .news-item .description p").attr("style", "");
				$("#newsWrapper").attr("style", "");
			}
		}

		cb();
	};

	NameSpace.News = News;
})(ESPN);

(function(NameSpace)
{
	var Filter = function() { return true };

	Filter.createInstance = function()
	{
		var Instance = new NameSpace.Filter();

		Instance.init();

		return Instance;
	};

	Filter.fn = Filter.prototype;

	Filter.fn.init = function()
	{
		this.sport = NameSpace.CurrentObj.sport;
	};

	Filter.fn.open = function()
	{
		var $filterBox = $("<div id='filter-box'></div>");
		var $filterWrap = $("<div id='filter-wrap'></div>");
		var $filterOptions = $("<div id='filter-options'><h1>Select a sport...</h1></div>");
		var $teamSelect = $("<select id='team-select'><option value=''>All Teams</option></select>")
		.change(function()
		{
			var arrayIndex = $("#sport-select :selected").data("index");
			var teamIndex = $("#team-select :selected").data("index");

			NameSpace.NewsObjs[arrayIndex].filterTitleTeam = $("#team-select :selected").html();
			NameSpace.NewsObjs[arrayIndex].teamColor = "#" + NameSpace.NewsObjs[arrayIndex].teamArray[teamIndex][3];
		});
		var $leagueSelect = $("<select id='league-select'><option value=''>All Leagues</option></select>")
		.change(function()
		{
			var sport = $sportSelect.val();

			if (sport != "tennis/" && sport != "football/" && sport != "basketball/" && sport != "hockey/")
				return true;

			var arrayIndex = $("#sport-select :selected").data("index");

			$filterBox.append("<div class='loading-screen'></div>");
			NameSpace.NewsObjs[arrayIndex].getAllTeams(sport, $(this).val(), function()
			{
				$filterBox.children(".loading-screen").remove();

				for (var i = 0; i < NameSpace.NewsObjs[arrayIndex].teamArray.length; i++)
				{
					var team = NameSpace.NewsObjs[arrayIndex].teamArray[i]

					$teamSelect.append("<option value='" + team[2] + "' data-index='" + i + "'>" + team[1] + " " + team[0] + "</option>");
				}

				$("#filter-options h1").html("Select a Team...");
				$leagueSelect.attr("disabled", "disabled");
				$leagueSelect.after($teamSelect);
				NameSpace.NewsObjs[arrayIndex].filterTitleLeague = $("#league-select :selected").html();
			});
		});
		var $sportSelect = $("<select autofocus id='sport-select'></select>")
		.change(function()
		{
			var arrayIndex = $("#sport-select :selected").data("index");

			$filterBox.append("<div class='loading-screen'></div>");
			NameSpace.NewsObjs[arrayIndex].getAllLeagues(function()
			{
				$filterBox.children(".loading-screen").remove();
				$sportSelect.attr("disabled", "disabled");
				if(NameSpace.NewsObjs[arrayIndex].leagueArray.length < 2 && NameSpace.NewsObjs[arrayIndex].sport  == "baseball/")
				{
					$filterBox.append("<div class='loading-screen'></div>");
					NameSpace.NewsObjs[arrayIndex].getAllTeams($sportSelect.val(), "mlb", function()
					{
						$filterBox.children(".loading-screen").remove();

						for (var i = 0; i < NameSpace.NewsObjs[arrayIndex].teamArray.length; i++)
						{
							var team = NameSpace.NewsObjs[arrayIndex].teamArray[i]

							$teamSelect.append("<option value='" + team[2] + "' data-index='" + i + "'>" + team[1] + " " + team[0] + "</option>");
						}

						$("#filter-options h1").html("Select a Team...");
						$sportSelect.after($teamSelect);
						NameSpace.NewsObjs[arrayIndex].filterTitleLeague = "Major League Baseball";
					});

					return;
				}
				else if(NameSpace.NewsObjs[arrayIndex].leagueArray.length < 2 && NameSpace.NewsObjs[arrayIndex].sport  == "hockey/")
				{
					$filterBox.append("<div class='loading-screen'></div>");
					NameSpace.NewsObjs[arrayIndex].getAllTeams($sportSelect.val(), "nhl", function()
					{
						$filterBox.children(".loading-screen").remove();

						for (var i = 0; i < NameSpace.NewsObjs[arrayIndex].teamArray.length; i++)
						{
							var team = NameSpace.NewsObjs[arrayIndex].teamArray[i]

							$teamSelect.append("<option value='" + team[2] + "' data-index='" + i + "'>" + team[1] + " " + team[0] + "</option>");
						}

						$("#filter-options h1").html("Select a Team...");
						$sportSelect.after($teamSelect);
						NameSpace.NewsObjs[arrayIndex].filterTitleLeague = "National Hockey League";
					});

					return;
				}

				for (var i = 0; i < NameSpace.NewsObjs[arrayIndex].leagueArray.length; i++)
				{
					var league = NameSpace.NewsObjs[arrayIndex].leagueArray[i]

					$leagueSelect.append("<option value='" + league[1] + "'>" + league[0] + "</option>");
				}

				$("#filter-options h1").html("Select a League...");
				$sportSelect.after($leagueSelect);
				NameSpace.NewsObjs[arrayIndex].filterTitleSport = $("#sport-select :selected").html();
			});
		});
		var $closeFilterBtn = $("<button id='close-filter-btn' class='select-btn'>Cancel</button>")
		.click(function()
		{
			$filterBox.remove();
		});
		var $filterDoneBtn = $("<button id='filter-done-btn'>Done</button>")
		.click(function()
		{
			var arrayIndex = $("#sport-select :selected").data("index");
			var league = "";
			var team = "";

			$filterBox.append("<div class='loading-screen'></div>");

			if (NameSpace.NewsObjs[arrayIndex].gotLeagues && $leagueSelect.val() != "")
			{
				league = $leagueSelect.val() + "/";
			}

			if (NameSpace.NewsObjs[arrayIndex].gotTeams && $teamSelect.val() != "")
			{
				team = $teamSelect.val();
			}

			NameSpace.NewsObjs[arrayIndex].getNews(league, team, function(){ $filterBox.remove(); });
		});

		for (var i = 0; i < NameSpace.NewsObjs.length; i++)
		{
			var obj = NameSpace.NewsObjs[i];

			if (obj.sport === "")
				obj.sport = "All Sports/";

			$sportSelect.append("<option data-index='" + i + "' value='" + obj.sport + "'>" + obj.sport.slice(0, -1) + "</option>");
		}

		$filterOptions.append($sportSelect);
		$filterWrap.append($closeFilterBtn, $filterDoneBtn, $filterOptions);
		$filterBox.append($filterWrap);
		$("#newsWrapper").append($filterBox);
	};

	NameSpace.Filter = Filter;
})(ESPN);
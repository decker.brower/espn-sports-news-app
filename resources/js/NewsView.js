var ESPN = ESPN || {};
ESPN.views = ESPN.views || {};
(function(APP, $, Backbone){
	APP.views.NewsView = Backbone.View.extend({
		el: $("#app-content"),
		template: _.template($("#news-item-template").html()),

		initialize: function() {

		},

		render: function() {
			var news = this.template({
				items: [
					{
						url: "http://www.google.com/",
						headline: "News Item",
						description: "News Item Description"
					},
					{
						url: "http://www.google.com/",
						headline: "News Item",
						description: "News Item Description"
					},
					{
						url: "http://www.google.com/",
						headline: "News Item",
						description: "News Item Description"
					}
				]
			});

			this.$el.html(news);
			return this;
		}
	});
})(ESPN, jQuery, Backbone);